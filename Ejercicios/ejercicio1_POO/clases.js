var Robot = /** @class */ (function () {
    function Robot() {
    }
    Robot.prototype.saludar = function () {
        return "Hola soy" + " " + this.name + this.apellido;
    };
    return Robot;
}());
var miRobot = new Robot();
miRobot.name = "Jorge";
miRobot.apellido = "Venegas";
console.log(miRobot.saludar());
