class Robot{
    //Atributos

    name : string;
    apellido : string;

    saludar() : string{
        return "Hola soy"+" " +this.name + this.apellido;
    }
    
}

let miRobot = new Robot();
miRobot.name = "Jorge";
miRobot.apellido = "Venegas";

console.log(miRobot.saludar());