let vAny:any =10;
let vUnknown: unknown = 10;

let s1 : string = vAny;
//let s2 : string = vUnknown;
let s2 : string = <string>vUnknown;// se puede usar "as"
//vAny.method();
//vUnknown.method();
console.log("tipos: "+ s1 +","+s2)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let empId:number = 1;
let empName:string = "Jorge";
let newEmployed:[number,string] = [1,"Jorge"];
newEmployed.push("Developer");

console.log(newEmployed);

///ENUMS
//let continents:string; mal

enum continents {
    NorthAmerica,
    SouthAmerica,
    Africa,
    Asia,
    Europa,
    Antartica,
    Australia
}
let myContinents = continents;
console.log("Enums: "+ myContinents.Africa);
///////clases

class Control{
    private state:any;
}

interface selectableControl extends Control{
    select():void;
}

class Button extends Control implements selectableControl{
    select(){
    }
}

class TextBox extends Control{
    select(){}
}

/* class Image implements selectableControl{//No es hijo de la clase control
    private state:any;
    select(){}
} */

