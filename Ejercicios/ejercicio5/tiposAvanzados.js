var somePosition = /** @class */ (function () {
    function somePosition() {
        this.x = 1;
        this.y = 2;
    }
    return somePosition;
}());
var somePosition2 = /** @class */ (function () {
    function somePosition2() {
        this.x = 1;
        this.y = 2;
    }
    return somePosition2;
}());
var position = { x: 1, y: 2 };
function nuevaEntrega() {
    return null;
}
var primeraEntrega = nuevaEntrega();
primeraEntrega.entregar();
primeraEntrega.firmar();
