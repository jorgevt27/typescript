var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var vAny = 10;
var vUnknown = 10;
var s1 = vAny;
//let s2 : string = vUnknown;
var s2 = vUnknown; // se puede usar "as"
//vAny.method();
//vUnknown.method();
console.log("tipos: " + s1 + "," + s2);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var empId = 1;
var empName = "Jorge";
var newEmployed = [1, "Jorge"];
newEmployed.push("Developer");
console.log(newEmployed);
///ENUMS
//let continents:string; mal
var continents;
(function (continents) {
    continents[continents["NorthAmerica"] = 0] = "NorthAmerica";
    continents[continents["SouthAmerica"] = 1] = "SouthAmerica";
    continents[continents["Africa"] = 2] = "Africa";
    continents[continents["Asia"] = 3] = "Asia";
    continents[continents["Europa"] = 4] = "Europa";
    continents[continents["Antartica"] = 5] = "Antartica";
    continents[continents["Australia"] = 6] = "Australia";
})(continents || (continents = {}));
var myContinents = continents;
console.log("Enums: " + myContinents.Africa);
///////clases
var Control = /** @class */ (function () {
    function Control() {
    }
    return Control;
}());
var Button = /** @class */ (function (_super) {
    __extends(Button, _super);
    function Button() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Button.prototype.select = function () {
    };
    return Button;
}(Control));
var TextBox = /** @class */ (function (_super) {
    __extends(TextBox, _super);
    function TextBox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextBox.prototype.select = function () { };
    return TextBox;
}(Control));
/* class Image implements selectableControl{//No es hijo de la clase control
    private state:any;
    select(){}
} */
