interface Positioning{
    x:number;
    y:number;
}
class somePosition implements Positioning{
    x = 1;
    y = 2;
}

type Position2={
    x:number;
    y:number;
}

class somePosition2 implements Position2{
    x = 1;
    y = 2;
}

type PartialPosition = {x:number;}|{y:number;};

/* class somePartialPosition implements PartialPosition{
    x = 1;
    y = 2;
} */

//
interface Coords {x:number;}
interface Coords {y:number;}
const position : Coords = {x : 1, y:2}

/* type Dimensions = {x:number;}
type Dimensions = {y:number;}
const dimensions : Dimensions = {x:1,y:2} */

//Intercepción
interface Paquete{
    tamañocaja : string;
    entregar(): void;
}
interface Carta{
    firmar():void;
    entregar():void;
}
function nuevaEntrega():Paquete & Carta{ // si usamos unión (|) solo permitiria usar el método firmar
    return null;
}
let primeraEntrega = nuevaEntrega();
primeraEntrega.entregar();
primeraEntrega.firmar();
//picks
interface one{
    prop:string
    prop3:string
}
interface dos{
    prop2:string
}

type prueba = Pick<Pick<dos,'prop2'>, 'prop2' >
type prueba1 = Pick<one,'prop'>&Pick<dos,'prop2'>