import React from 'react';
import { Router } from '@reach/router';
import Layout from '../Layout/Layout';
import Home from '../../pages/Home';
import HeroeNew from '../../pages/HeroeNew';



const App = () => {
  return (
    <Layout> 
      <Router>
        <Home path="/" />
        <HeroeNew path="/heroeNew" />
      </Router>
    </Layout>
  );
};
export default App;
/* 

import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import HeroeDetail from '../../pages/HeroeDetail';
import HeroeEdit from '../../pages/HeroeEdit';
import HeroeNew from '../../pages/HeroeNew';
import Heroes from '../../pages/Heroes';
import NotFound from '../../pages/NotFound';
import Layout from '../Layout/Layout';

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path="/" component={Heroes} />
            <Route exact path="/heroe/new" component={HeroeNew} />
            <Route exact path="/heroe/:heroeId/edit" component={HeroeEdit} />
            <Route
              exact
              path="/heroe/:heroeId/detail"
              component={HeroeDetail}
            />
            <Route component={NotFound} />
          </Switch>
        </Layout>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
 */