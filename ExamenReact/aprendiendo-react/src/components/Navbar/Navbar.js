import React from 'react';
import '../../styles/Navbar.css';
import logo from '../../images/marvel-logo.png';

function Navbar() {
  return (
    <nav className="navbar navbar-custom">
      <div className="container">
          <img src={logo} alt="" />
      </div>
    </nav>
  );
}

export default Navbar;
