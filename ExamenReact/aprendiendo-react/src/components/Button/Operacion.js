
//import api from '../utils/api';
import api from '../../utils/api';

const Operacion = (state, action) => {
  let nuevoEstado = { ...state };

  switch (action.type) {
    case 'GUARDAR':
      api.heroes.createHeroe(state.form);
      nuevoEstado.estado = true;
      console.log("Ejecuto Guardar");
      break;

    default:
      break;
  }

  return nuevoEstado;
};

export default Operacion;