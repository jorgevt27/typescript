import React from 'react';
import { Link } from '@reach/router';
/* import { Link } from 'react-router-dom';
 */

const ButtonNewHero = () =>{

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12 text-right">
          <Link to="/heroeNew" className="btn btn-primary ml-auto">
            Nuevo
          </Link>
        </div>
      </div>
    </div>
  );

};
export default ButtonNewHero;

/* function ButtonNewHero(props) {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12 text-right">
          <Link to="/heroeNew" className="btn btn-primary ml-auto">
            Nuevo
          </Link>
        </div>
      </div>
    </div>
  );
}

export default ButtonNewHero; */
