import React from 'react';
import ButtonNewHero from '../components/Button/ButtonNewHero';

const Home = () => {
  return (
    <React.Fragment>
      <div className="container">Inicio</div>
      <ButtonNewHero />
    </React.Fragment>
  );
};

export default Home;
