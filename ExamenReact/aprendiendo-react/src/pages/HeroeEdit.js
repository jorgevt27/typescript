import React from 'react';
import HeroeForm from '../components/Heroe/HeroeForm';
import HeroePreview from '../components/Heroe/HeroePreview';
import LoaderPage from '../components/Loader/LoaderPage';
import api from '../utils/api';

export default class HeroeEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      heroeId: props.match.params.heroeId,
      loading: false,
      error: null,
      form: {
        Company: '',
        Name: '',
        Movie: '',
        PhotoUrl: '',
      },
      validationMessages: {
        Company: undefined,
        Name: undefined,
        Movie: undefined,
        PhotoUrl: undefined,
      },
    };
  }

  componentDidMount() {
    this.getDataHeroes();
  }

  getDataHeroes = async () => {
    this.setState({ loading: true, error: null });
    try {
      const dataHeroe = await api.heroes.getHeroe(this.state.heroeId);
      this.setState({ form: dataHeroe, loading: false });
      console.log(this.state.form);
    } catch (error) {
      this.setState({ error: error, loading: false });
    }
  };

  render() {
    if (this.state.loading) {
      return <LoaderPage />;
    }
    return (
      <div className="container">
        <div className="row">
          <HeroeForm
            onChangeInput={this.handleChange}
            onSave={this.handleValidateForm}
            formValues={this.state.form}
            onBack={this.handleGoBack}
            errorForm={this.state.error}
            validationMessage={this.state.validationMessages}
            modalIsOpen={this.state.modalIsOpen}
            onCloseModal={this.onCloseModal}
            onRedirectToHeroes={this.onRedirectToHeroes}
          />
          <HeroePreview
            company={this.state.form.Company || 'COMPANY NAME'}
            name={this.state.form.Name || 'HEROE NAME'}
            movie={this.state.form.Movie || 'MOVIE NAME'}
            photoUrl={
              this.state.form.PhotoUrl ||
              'https://i.pinimg.com/originals/b5/34/df/b534df05c4b06ebd32159b2f9325d83f.jpg'
            }
          />
        </div>
      </div>
    );
  }

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleGoBack = () => {
    this.props.history.goBack();
  };

  handleEditHeroe = async () => {
    this.setState({ loading: true, error: null });
    try {
      await api.heroes.updateHeroe(this.state.heroeId, this.state.form);
      this.setState({ loading: false, modalIsOpen: true });
    } catch (error) {
      this.setState({ loading: false, error: error });
    }
  };

  handleValidateForm = () => {
    let flagExecution = true;
    let messageCompany = undefined;
    let messageName = undefined;
    let messageMovie = undefined;
    let messagePhotoUrl = undefined;

    if (this.state.form.Company === '') {
      messageCompany = 'Este campo es obligatorio';
      flagExecution = false;
    }
    if (this.state.form.Name === '') {
      messageName = 'Este campo es obligatorio';
      flagExecution = false;
    }
    if (this.state.form.Movie === '') {
      messageMovie = 'Este campo es obligatorio';
      flagExecution = false;
    }
    if (this.state.form.PhotoUrl === '') {
      messagePhotoUrl = 'Este campo es obligatorio';
      flagExecution = false;
    }

    if (flagExecution) {
      this.handleEditHeroe();
    } else {
      this.setState({
        validationMessages: {
          Company: messageCompany,
          Name: messageName,
          Movie: messageMovie,
          PhotoUrl: messagePhotoUrl,
        },
      });
    }
  };

  onCloseModal = () => {
    this.props.history.push('/');
  };

  onRedirectToHeroes = () => {
    this.props.history.push('/');
  };
}
