import React, { Component } from 'react';
import HeroeDetailHtml from '../components/Heroe/HeroeDetailHtml';
import LoaderPage from '../components/Loader/LoaderPage';
import api from '../utils/api';

export default class HeroeDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heroeId: props.match.params.heroeId,
      loading: true,
      heroe: undefined,
      error: null,
      modalIsOpen: false,
    };
  }

  componentDidMount() {
    this.getDataHeroe();
  }

  getDataHeroe = async () => {
    try {
      this.setState({ loading: true, error: null });
      const dataHeroe = await api.heroes.getHeroe(this.state.heroeId);
      this.setState({ loading: false, heroe: dataHeroe });
    } catch (error) {
      this.setState({ loading: false, error: error });
    }
  };

  onOpenModal = () => {
    this.setState({ modalIsOpen: true });
  };

  onCloseModal = () => {
    this.setState({ modalIsOpen: false });
  };

  render() {
    if (this.state.loading) {
      return <LoaderPage />;
    }
    return (
      <HeroeDetailHtml
        heroe={this.state.heroe}
        modalIsOpen={this.state.modalIsOpen}
        onOpenModal={this.onOpenModal}
        onCloseModal={this.onCloseModal}
        onDeleteHeroe={this.deleteHeroe}
      />
    );
  }

  deleteHeroe = async () => {
    try {
      this.setState({ loading: true, error: null });
      await api.heroes.removeHeroe(this.state.heroeId);
      this.setState({ loading: false });
      this.props.history.push('/');
    } catch (error) {
      this.setState({ loading: false, error: error });
    }
  };
}
