import React, { useState } from 'react';
import HeroeForm from '../components/Heroe/HeroeForm';
import HeroePreview from '../components/Heroe/HeroePreview';
import LoaderPage from '../components/Loader/LoaderPage';
import api from '../utils/api';

const HeroeNew = () =>{
  
  const [valorCompany, setValorCompany] = useState('');
  const [valorName, setValorName] = useState('');
  const [valorMovie, setValorMovie] = useState('');
  const [valorPhotoUrl, setValorPhotoUrl] = useState('');


  const onChangeCompany = (e) => {
    setValorCompany(e.target.value);
  };
  const onChangeName = (e) => {
    setValorName(e.target.value);
  };
  const onChangeMovie = (e) => {
    setValorMovie(e.target.value);
  };
  const onChangePhotoUrl = (e) => {
    setValorPhotoUrl(e.target.value);
  };

  return (
    <div className="container">
    <div className="row">
      <HeroeForm 
          valorCompany={valorCompany} 
          valorName={valorName} 
          valorMovie={valorMovie} 
          valorPhotoUrl={valorPhotoUrl}
          onChangeCompany={onChangeCompany} 
          onChangeName={onChangeName}
          onChangeMovie={onChangeMovie} 
          onChangePhotoUrl={onChangePhotoUrl}
       />
    </div>
  </div>
  );

};
export default HeroeNew;

/* export default class HeroeNew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      loading: false,
      error: null,
      form: {
        Company: '',
        Name: '',
        Movie: '',
        PhotoUrl: '',
      },
      validationMessages: {
        Company: undefined,
        Name: undefined,
        Movie: undefined,
        PhotoUrl: undefined,
      },
    };
  }
  render() {
    if (this.state.loading) {
      return <LoaderPage />;
    }
    return (
      <div className="container">
        <div className="row">
          <HeroeForm
            onChangeInput={this.handleChange}
            formValues={this.state.form}
            onBack={this.handleGoBack}
            onSave={this.handleValidateForm}
            errorForm={this.state.error}
            validationMessage={this.state.validationMessages}
            modalIsOpen={this.state.modalIsOpen}
            onCloseModal={this.onCloseModal}
            onRedirectToHeroes={this.onRedirectToHeroes}
          />
          <HeroePreview
            company={this.state.form.Company || 'COMPANY NAME'}
            name={this.state.form.Name || 'HEROE NAME'}
            movie={this.state.form.Movie || 'MOVIE NAME'}
            photoUrl={
              this.state.form.PhotoUrl ||
              'https://i.pinimg.com/originals/b5/34/df/b534df05c4b06ebd32159b2f9325d83f.jpg'
            }
          />
        </div>
      </div>
    );
  }

  handleCreateHeroe = async () => {
    this.setState({ loading: true, error: null });
    try {
      await api.heroes.createHeroe(this.state.form);
      this.setState({ loading: false, modalIsOpen: true, error: null });
    } catch (error) {
      this.setState({ loading: false, error: error });
    }
  };

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleValidateForm = () => {
    let flagExecution = true;
    let messageCompany = undefined;
    let messageName = undefined;
    let messageMovie = undefined;
    let messagePhotoUrl = undefined;

    if (this.state.form.Company === '') {
      messageCompany = 'Este campo es obligatorio';
      flagExecution = false;
    }
    if (this.state.form.Name === '') {
      messageName = 'Este campo es obligatorio';
      flagExecution = false;
    }
    if (this.state.form.Movie === '') {
      messageMovie = 'Este campo es obligatorio';
      flagExecution = false;
    }
    if (this.state.form.PhotoUrl === '') {
      messagePhotoUrl = 'Este campo es obligatorio';
      flagExecution = false;
    }

    if (flagExecution) {
      this.handleCreateHeroe();
    } else {
      this.setState({
        validationMessages: {
          Company: messageCompany,
          Name: messageName,
          Movie: messageMovie,
          PhotoUrl: messagePhotoUrl,
        },
      });
    }
  };

  handleGoBack = () => {
    this.props.history.goBack();
  };

  onCloseModal = () => {
    this.props.history.push('/');
  };

  onRedirectToHeroes = () => {
    this.props.history.push('/');
  };
}
 */