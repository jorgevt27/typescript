import React, {useState} from 'react';
import HeroeList from '../components/Heroe/HeroeList';
import MessageWarning from '../components/Messages/MessageWarning';
import ButtonNewHero from '../components/Button/ButtonNewHero';
import LoaderHeroes from '../components/Loader/LoaderHeroes';
import api from '../utils/api';


const Heroes = () =>{

  return (
    <div>
      <ButtonNewHero />
    </div>
  );

};
export default Heroes;



//eliminar 

/* export default class Heroes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: null,
      heroes: undefined,
    };
  }

  componentDidMount() {
    this.getDataHeroes();
  }

  getDataHeroes = async () => {
    this.setState({ loading: true, error: null });
    try {
      const dataHeroes = await api.heroes.listHeroes();
      this.setState({ loading: false, heroes: dataHeroes });
    } catch (error) {
      this.setState({ loading: false, error: error });
    }
  };

  render() {
    if (this.state.loading) {
      return (
        <React.Fragment>
          <ButtonNewHero />
          <br />
          <LoaderHeroes />
        </React.Fragment>
      );
    }

    if (this.state.error) {
      return <MessageWarning message={this.state.error.message} />;
    }

    if (!this.state.heroes || this.state.heroes.length === 0) {
      return (
        <div>
          <ButtonNewHero />
          <br />
          <MessageWarning message="No existe información." />;
        </div>
      );
    }

    return (
      <div>
        <ButtonNewHero />
        <br />
        <HeroeList heroes={this.state.heroes} />
      </div>
    );
  }
}
 */