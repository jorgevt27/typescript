"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.Snake = void 0;
var Snake = /** @class */ (function () {
    function Snake(initialVelocity, initialPosition, initialLength, render) {
        this.velocity = initialVelocity;
        this.head = initialPosition;
        this.body = [];
        var x = initialPosition.x - initialVelocity.dx;
        var y = initialPosition.y - initialVelocity.dy;
        for (var i = 0; i < initialLength - 1; i++) {
            this.body.push({ x: x, y: y });
            y -= initialVelocity.dy;
            x -= initialVelocity.dx;
        }
        this.render = render;
    }
    Snake.prototype.calculateLimitPosition = function (board) {
        var limitPosition = {};
        // wrap snake position horizontally on edge of the board
        if (this.head.x < 0) {
            limitPosition.x = board.getWidth() - board.getGridSize();
        }
        else if (this.head.x >= board.getWidth()) {
            limitPosition.x = 0;
        }
        // wrap snake position vertically on edge of the board
        if (this.head.y < 0) {
            limitPosition.y = board.getHeight() - board.getGridSize();
        }
        else if (this.head.y >= board.getHeight()) {
            limitPosition.y = 0;
        }
        return limitPosition;
    };
    Snake.prototype.updateCells = function () {
        this.body.unshift({ x: this.head.x, y: this.head.y });
        this.head.x += this.velocity.dx;
        this.head.y += this.velocity.dy;
    };
    /**
     * Moves the Snake One position removing the last cell and creating a new head
     */
    Snake.prototype.move = function (board) {
        this.updateCells();
        this.body.pop();
        this.head = __assign(__assign({}, this.head), this.calculateLimitPosition(board));
    };
    /**
     * Retrieves the whole snake cells
     */
    Snake.prototype.getCells = function () {
        return __spreadArrays([this.head], this.body);
    };
    /**
     * Retrieves the Snake direction Vector
     */
    Snake.prototype.getDirection = function () {
        return this.velocity;
    };
    /**
     * Changes the Snake direction
     * @param newDirection the new Snake direction
     */
    Snake.prototype.changeDirection = function (newDirection) {
        this.velocity = newDirection;
    };
    Snake.prototype.draw = function () {
        this.render.drawSnakeCell(this.getCells(), 'green');
    };
    Snake.prototype.feed = function () {
        this.updateCells();
    };
    return Snake;
}());
exports.Snake = Snake;
