"use strict";
exports.__esModule = true;
exports.Game = void 0;
var board_js_1 = require("./board.js");
var snake_js_1 = require("./snake.js");
var Game = /** @class */ (function () {
    function Game() {
        var canvas = document.getElementById('game');
        this.board = new board_js_1.Board(canvas);
        this.snake = new snake_js_1.Snake({ dx: Game.grid, dy: 0 }, { x: 160, y: 160 }, 4);
    }
    Game.prototype.start = function () {
        requestAnimationFrame(this.start.bind(this));
        this.snake.move();
        this.board.draw(this.snake, Game.grid);
    };
    Game.grid = 16;
    return Game;
}());
exports.Game = Game;
