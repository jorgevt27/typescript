"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.Snake = void 0;
var Snake = /** @class */ (function () {
    function Snake(initialVelocity, initialPosition, initialLength) {
        this.velocity = initialVelocity;
        this.head = initialPosition;
        this.body = [];
        var x = initialPosition.x - initialVelocity.dx;
        var y = initialPosition.y - initialVelocity.dy;
        for (var i = 0; i < initialLength - 1; i++) {
            this.body.push({ x: x, y: y });
            y -= initialVelocity.dy;
            x -= initialVelocity.dx;
        }
    }
    Snake.prototype.move = function () {
        this.body.unshift({ x: this.head.x, y: this.head.y });
        this.body.pop();
        this.head.x += this.velocity.dx;
        this.head.y += this.velocity.dy;
    };
    Snake.prototype.getCells = function () {
        return __spreadArrays([this.head], this.body);
    };
    return Snake;
}());
exports.Snake = Snake;
