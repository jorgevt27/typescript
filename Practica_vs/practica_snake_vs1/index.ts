import {Game} from "./game.js";

let game;

window.onload = (ev : Event) =>{
    game = new Game();
    game.start();
}