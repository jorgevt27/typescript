import { Board } from "./board.js";
import { Snake } from "./snake.js";
export class Game {
    constructor() {
        const canvas = document.getElementById('game');
        this.board = new Board(canvas);
        this.snake = new Snake({ dx: Game.grid, dy: 0 }, { x: 160, y: 160 }, 4);
    }
    start() {
        requestAnimationFrame(this.start.bind(this));
        this.snake.move();
        this.board.draw(this.snake, Game.grid);
    }
}
Game.grid = 16;
