export class Board {
    constructor(canvas) {
        this.context = canvas.getContext('2d');
        this.width = canvas.width;
        this.heigth = canvas.height;
    }
    draw(snake, grid) {
        this.clear();
        if (this.context)
            this.context.fillStyle = 'green';
        snake.getCells().forEach(c => {
            this.context?.fillRect(c.x, c.y, grid - 1, grid - 1);
        });
    }
    clear() {
        this.context?.clearRect(0, 0, this.width, this.heigth);
    }
}
