export class Snake {
    constructor(initialVelocity, initialPosition, initialLength) {
        this.velocity = initialVelocity;
        this.head = initialPosition;
        this.body = [];
        let x = initialPosition.x - initialVelocity.dx;
        let y = initialPosition.y - initialVelocity.dy;
        for (let i = 0; i < initialLength - 1; i++) {
            this.body.push({ x: x, y: y });
            y -= initialVelocity.dy;
            x -= initialVelocity.dx;
        }
    }
    move() {
        this.body.unshift({ x: this.head.x, y: this.head.y });
        this.body.pop();
        this.head.x += this.velocity.dx;
        this.head.y += this.velocity.dy;
    }
    getCells() {
        return [this.head, ...this.body];
    }
}
