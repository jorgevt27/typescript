export interface Point {
    x: number,
    y: number
}

export interface Vector {
    dx: number,
    dy: number
}

