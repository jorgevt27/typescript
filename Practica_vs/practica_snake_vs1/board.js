"use strict";
exports.__esModule = true;
exports.Board = void 0;
var Board = /** @class */ (function () {
    function Board(canvas) {
        this.context = canvas.getContext('2d');
        this.width = canvas.width;
        this.heigth = canvas.height;
    }
    Board.prototype.draw = function (snake, grid) {
        var _this = this;
        this.clear();
        if (this.context)
            this.context.fillStyle = 'green';
        snake.getCells().forEach(function (c) {
            var _a;
            (_a = _this.context) === null || _a === void 0 ? void 0 : _a.fillRect(c.x, c.y, grid - 1, grid - 1);
        });
    };
    Board.prototype.clear = function () {
        var _a;
        (_a = this.context) === null || _a === void 0 ? void 0 : _a.clearRect(0, 0, this.width, this.heigth);
    };
    return Board;
}());
exports.Board = Board;
