export class Snake {
    constructor(initialVelocity, initialPosition, initialLength) {
        this.velocity = initialVelocity;
        this.head = initialPosition;
        this.body = [];
        let x = initialPosition.x - initialVelocity.dx;
        let y = initialPosition.y - initialVelocity.dy;
        for (let i = 0; i < initialLength - 1; i++) {
            this.body.push({ x: x, y: y });
            y -= initialVelocity.dy;
            x -= initialVelocity.dx;
        }
    }
    move() {
        this.body.unshift({ x: this.head.x, y: this.head.y });
        this.body.pop();
        this.head.x += this.velocity.dx;
        this.head.y += this.velocity.dy;
    }
    getCells() {
        return [this.head, ...this.body];
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic25ha2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zbmFrZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQSxNQUFNLE9BQU8sS0FBSztJQU1kLFlBQVksZUFBdUIsRUFBRSxlQUFzQixFQUFFLGFBQXFCO1FBQzlFLElBQUksQ0FBQyxRQUFRLEdBQUcsZUFBZSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBZSxDQUFDO1FBQzVCLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxDQUFDLEdBQVcsZUFBZSxDQUFDLENBQUMsR0FBRyxlQUFlLENBQUMsRUFBRSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxHQUFXLGVBQWUsQ0FBQyxDQUFDLEdBQUcsZUFBZSxDQUFDLEVBQUUsQ0FBQztRQUN2RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDL0IsQ0FBQyxJQUFJLGVBQWUsQ0FBQyxFQUFFLENBQUM7WUFDeEIsQ0FBQyxJQUFJLGVBQWUsQ0FBQyxFQUFFLENBQUM7U0FDM0I7SUFDTCxDQUFDO0lBRUQsSUFBSTtRQUNBLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBRUQsUUFBUTtRQUNKLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBvaW50LCBWZWN0b3J9IGZyb20gXCIuL21vZGVscy9tb2RlbHNcIjtcblxuXG5leHBvcnQgY2xhc3MgU25ha2V7XG5cbiAgICBwcml2YXRlIGhlYWQ6IFBvaW50O1xuICAgIHByaXZhdGUgdmVsb2NpdHk6IFZlY3RvcjtcbiAgICBwcml2YXRlIGJvZHk6IFBvaW50W107XG5cbiAgICBjb25zdHJ1Y3Rvcihpbml0aWFsVmVsb2NpdHk6IFZlY3RvciwgaW5pdGlhbFBvc2l0aW9uOiBQb2ludCwgaW5pdGlhbExlbmd0aDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMudmVsb2NpdHkgPSBpbml0aWFsVmVsb2NpdHk7XG4gICAgICAgIHRoaXMuaGVhZCA9IGluaXRpYWxQb3NpdGlvbjtcbiAgICAgICAgdGhpcy5ib2R5ID0gW107XG4gICAgICAgIGxldCB4OiBudW1iZXIgPSBpbml0aWFsUG9zaXRpb24ueCAtIGluaXRpYWxWZWxvY2l0eS5keDtcbiAgICAgICAgbGV0IHk6IG51bWJlciA9IGluaXRpYWxQb3NpdGlvbi55IC0gaW5pdGlhbFZlbG9jaXR5LmR5O1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGluaXRpYWxMZW5ndGggLSAxOyBpKyspIHtcbiAgICAgICAgICAgIHRoaXMuYm9keS5wdXNoKHsgeDogeCwgeTogeSB9KTtcbiAgICAgICAgICAgIHkgLT0gaW5pdGlhbFZlbG9jaXR5LmR5O1xuICAgICAgICAgICAgeCAtPSBpbml0aWFsVmVsb2NpdHkuZHg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBtb3ZlKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmJvZHkudW5zaGlmdCh7IHg6IHRoaXMuaGVhZC54LCB5OiB0aGlzLmhlYWQueSB9KTtcbiAgICAgICAgdGhpcy5ib2R5LnBvcCgpO1xuICAgICAgICB0aGlzLmhlYWQueCArPSB0aGlzLnZlbG9jaXR5LmR4O1xuICAgICAgICB0aGlzLmhlYWQueSArPSB0aGlzLnZlbG9jaXR5LmR5O1xuICAgIH1cblxuICAgIGdldENlbGxzKCk6IFBvaW50W10ge1xuICAgICAgICByZXR1cm4gW3RoaXMuaGVhZCwgLi4udGhpcy5ib2R5XTtcbiAgICB9XG59Il19