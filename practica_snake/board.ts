import {Snake} from "./snake.js";

export class Board{

    private context:CanvasRenderingContext2D | null;
    private width:number;
    private heigth:number;

    constructor(canvas: HTMLCanvasElement) {
        this.context = canvas.getContext('2d');
        this.width = canvas.width;
        this.heigth=canvas.height;
    }

    draw(snake:Snake,grid:number):void{
        this.clear();
        if(this.context) this.context.fillStyle = 'green';
        snake.getCells().forEach(c => {
            this.context?.fillRect(c.x,c.y,grid-1,grid-1);
        });
    }
    clear():void{
        this.context?.clearRect(0,0,this.width,this.heigth);
    }

}